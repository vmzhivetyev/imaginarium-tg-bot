import flickrapi
import random
import json

def photourl(p):
	return 'https://farm{farm}.staticflickr.com/{server}/{id}_{secret}.jpg'.format(**p)

def getImagesJson(count, page=1):
	api_key = u'734a78ccaac2d7a4f363b6961ffd8b60'
	api_secret = u'f79cccf381e9cf09'

	flickr = flickrapi.FlickrAPI(api_key, api_secret, format='json')
	photos = flickr.photos.search(text='surreal', page=page, per_page=str(count), sort='relevance')

	parsed = json.loads(photos.decode('utf-8'))

	j = json.dumps(parsed, indent=4, sort_keys=True)
	print('FLICKR PAGE ' + str(page) + ' :\n' + j)

	return parsed

def getImages(count):
	# return []

	parsed = getImagesJson(count)

	pages = int(parsed['photos']['pages'] / 10)
	page = random.randint(1,int(4000/count))

	parsed = getImagesJson(count, page)

	print('retrieved images page {}/{}'.format(page, pages))

	photos = parsed['photos']['photo']

	for photo in photos:
		photo['url'] = photourl(photo)

	return photos