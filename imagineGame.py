import json
import tools
import random
import tgAPI
import imagine

def d(text):
	if text[:1] != '/':
		return -1

	digit = text[1:]
	if digit.isdigit():
		return int(digit)
	else:
		return -1

def r(array):
	if len(array) == 0:
		return None
	if len(array) == 1:
		return array[0]
	return array[random.randint(0,len(array) - 1)]


class ImagineGame:
	cardsInHand = 3
	totalSteps = 10

	stickers = {
		'памагити' : 'CAADAgADDQEAAhZ8aAOCt9jZTPHL_QI',
		'привет' : [
			'CAADAgADlQEAAoo0zQhliFEG91oaLgI', # приветы
			'CAADAgADfgcAAhhC7gifOrw7y6eFQgI', # кошка с ноутом HI
			'CAADAgADegIAAu7EoQrtbjqTdNbWTQI', # волк привет
			'CAADAgADgQIAAnCO7Q4xpAskFN1lygI', # РКН
			'CAADAgADUAADmS9LCgneeMhlJCKvAg', # патриарх
			'CAADAgAD9QIAApzW5woDP3qDEC4ObwI', # вейдер
			'CAADAgADlwYAAvoLtgiTap3uNdUieAI', # енот шляпу снимает
			'CAADAgADUAADmS9LCgneeMhlJCKvAg', # hi human котик
			],
		'енот' : [
			'CAADAgADUgYAAvoLtggTEf7tmk2OCgI', # енот перфект
			'CAADAgADeQYAAvoLtgjYIpDlXr_9iQI', # енот ждет
			],
		'gg' : [
			'CAADAgADDwQAAqtWmgwD1cbz7Rn7swI', # 0 дней никто не ливал
		]
	}
	

	def __init__(self, chatId, tgApi):
		self.tgApi = tgApi
		self.chatId = chatId
		self.tgApi.addMessageListener(self.onMessage)
		self.tgApi.addCallbackDataListener(self.onCallback)
		self.tgApi.sendMessage(self.chatId, 'Game initialized. \nOther players should send me the number to enter the game. Send /play if all players are registered.')
		self.tgApi.sendMessage(self.chatId, str(self.chatId))
		self.players = []
		self.playerIndex = 0
		self.state = 'рега'
		self.images = []
		self.table = []

	def destroy(self):
		self.tgApi.removeListener(self.onMessage)
		self.tgApi.removeListener(self.onCallback)

	def player(self, p):
		real = [x for x in self.players if x.id == p.id]

		if len(real) == 0:
			return None
		else:
			return real[0]

	def leader(self):
		return self.players[self.playerIndex]

	def addPlayer(self, player):
		exists = self.player(player) is not None
		if exists:
			self.tgApi.sendMessage(player.id, 'Хули дважды лезешь, пидор?!')
			return

		self.players.append(player)
		self.broadcastSticker('привет')
		self.broadcast("Welcome, " + player.call())

	def sendPlayerList(self):
		self.broadcast(str(len(self.players)) + ' players: \n' + '\n'.join([x.call() for x in self.players]))

	def getImages(self):

		self.broadcast('Getting images...')
		
		playersCount = len(self.players)
		picturesCount = ( self.cardsInHand + self.totalSteps ) * playersCount

		self.images = imagine.getImages(picturesCount)

		info = 'Loaded ' + str(len(self.images)) + ' images.'
		self.broadcast(info)
		tools.log(info)

	def updateHands(self):
		for p in self.players:
			p.select = None

			if p.pick is not None:
				p.hand.remove(p.pick)
				p.pick = None

			while len(p.hand) < self.cardsInHand and len(self.images) > 0:
				pic = r(self.images)
				self.images.remove(pic)
				p.hand.append(pic['url'])

			if len(self.images) == 0:
				self.broadcast('Cards ended!')

			# tools.log(p.name() + ' has '  + str(p.hand))

	def allPicked(self):
		for p in self.players:
			if p.pick is None:
				return False
		return True

	def allSelected(self):
		for p in self.players:
			if p != self.leader() and p.select is None:
				return False
		return True

	def resend(self, msg):
		names = [x.name() for x in self.players]
		tools.log(self.chatId,'> Forwarding message',msg,'to',names)

		for p in self.players:
			if p.id != msg.sender.id:
				self.tgApi.forwardMessage(p.id, msg)

	def broadcast(self, *args):
		for receiver in self.players:
			self.tgApi.sendMessage(receiver.id, *args)

	def broadcastSticker(self, stickerName):
		stickerId = self.stickers.get(stickerName) # may be array

		# get random sticker if it's an array
		if type(stickerId) is list:
			if len(stickerId) == 1:
				stickerId = stickerId[0]
			elif len(stickerId) > 1:
				stickerId = r(stickerId)

		for receiver in self.players:
			self.tgApi.sendSticker(receiver.id, stickerId)

	def broadcastTable(self):
		pics = [x.pick for x in self.players]
		tools.log('pics to shuffle ',pics)
		random.shuffle(pics)
		tools.log('shuffled pics ',pics)
		self.table = pics

		tools.log('table:',self.table)

		for receiver in self.players:
			if receiver.id != self.leader().id:
				self.sendHand(receiver, pics)
			else:
				self.tgApi.sendMessage(receiver.id, 'Now other players are adding their picktures to the pool...')

	def broadcastSelects(self):

		winners = [x for x in self.players if x.select == self.leader().pick]
		allWinners = len(winners) == len(self.players) - 1

		# угадали все
		if allWinners:
			# ведущий лох
			# всем по три
			for winner in winners:
				winner.score += 3

		else:
			# угадали не все
			if len(winners) > 0:
				# ведущему три
				self.leader().score += 3
				# всем по три
				for winner in winners:
					winner.score += 3

			# каждому за голос на его карту

		for pic in self.table:
			# counting
			authorUser = [x for x in self.players if x.pick == pic][0]
			votersUsers = [x for x in self.players if x.select == pic]
			isSecret = authorUser == self.leader()

			if not allWinners:
				authorUser.score += len(votersUsers)

			# texting
			author = authorUser.call()
			voters = [x.call() for x in votersUsers]

			if voters == []:
				voters = '0'
			else:
				voters = '\n' + '\n'.join(voters)

			for p in self.players:
				caption = ('✅ ' if isSecret else '') + author + '\n\n'
				caption = caption + 'Voters: ' + voters
				self.tgApi.sendPhoto(p.id, pic, caption)

	def broadcastScores(self):
		scores = [[x.call(), x.score] for x in self.players]
		scores = sorted(scores, key=lambda k:k[1])
		scores = [str(x[1]) + ' ' + x[0] for x in scores]
		scores = '\n'.join(scores)
		self.broadcast('Scores:\n'+scores)

	def sendHand(self, player, pics='hand'):
		if pics == 'hand':
			pics = player.hand

		if not pics:
			tools.log('PICS IS EMPTY')
			self.tgApi.sendMessage(player.id, 'Oops, can\'t get your pics -_-')

		if len(pics) == 0:
			self.tgApi.sendMessage(player.id, 'No cards in hand...')

		i = 1
		for url in pics:
			if player.pick != url:
				self.tgApi.sendPhoto(player.id, url, '/' + str(i))
			i = i + 1


	def welcomeLeader(self):
		leader = self.players[self.playerIndex]

		self.broadcast(leader.call() + ' is now picking a picture...')
		self.tgApi.sendMessage(leader.id, 'Pick one of the next pictures and then write your association!')
		self.sendHand(leader)

	def nextLeader(self):
		self.state = 'лидер пикает'

		self.playerIndex += 1
		if self.playerIndex >= len(self.players):
			self.playerIndex = 0

		self.updateHands()
		self.welcomeLeader()


	def onCallback(self, chatId, data, sender):
		if data.split(' ')[0] == 'mind':
			cid = data.split(' ')[1]
			chat = self.tgApi.getChat(cid)
			self.minds[chatId] = cid

			self.tgApi.sendMessage(chatId, 'You are now controlling chat {}'.format(chat['title'] if 'title' in chat else chat['first_name'] + ' ' + chat['last_name']))
			tools.log('{} is now controlling chat {}'.format(sender.name(), chat))

	def onMessage(self, msg):
		if self.state == 'рега':

			# регаем юзера
			if msg.text == str(self.chatId):
				self.addPlayer(msg.sender)
				return

			# стартуем игру
			elif '/play' == msg.text and self.chatId == msg.chatId:
				self.getImages()
				if len(self.images) is 0:
					self.broadcast('Sorry, for some reason I can\'t get images to play with')
					self.broadcastSticker('памагити')
				else:
					self.state = 'лидер пикает'

					self.updateHands()

					#self.playerIndex = random.randint(0,len(self.players) - 1)

					self.sendPlayerList()

					self.broadcast(self.players[0].call() + ' has started the game!')
					self.welcomeLeader()

		# дальше только участники и во время игры

		# выдергиваем настоящего учатника отправителя
		
		msg.sender = self.player(msg.sender)
		if msg.sender is None:
			return

		# если не команда
		if msg.text[:1] != '/':
			self.resend(msg)
		else:
			# это команда
			leader = self.leader()

			if msg.text == '/score':
				self.broadcastScores()
				return

			if msg.text == '/gg':
				self.broadcastSticker('gg')
				self.broadcast(msg.sender.call() + ' leaved the game')
				self.players.remove(msg.sender)

			if self.state == 'лидер пикает':
				if msg.sender.id == leader.id:
					digit = d(msg.text)

					if digit >= 1 and digit <= len(leader.hand):
						leader.pick = leader.hand[digit - 1]
						self.state = 'остальные подкидывают'
						self.broadcast('Now the leader writes an association and others should add one of their own pictures corresponding to that association.')
						for p in self.players:
							if p.id != leader.id:
								self.sendHand(p)

					# else:
					# 	self.tgApi.sendMessage(leader.id, ':x: That\'s a wrong number')

			elif self.state == 'остальные подкидывают':
				if msg.sender.pick is None:
					digit = d(msg.text)

					if digit >= 1 and digit <= len(msg.sender.hand):
						msg.sender.pick = msg.sender.hand[digit - 1]

						if self.allPicked():
							self.broadcast('Now select the picture you think was dropped by ' + self.leader().name())
							self.broadcastTable()
							self.state = 'выбор со стола'

			elif self.state == 'выбор со стола':
				digit = d(msg.text)
				if digit >= 1 and digit <= len(self.table):
					if self.table[digit - 1] == msg.sender.pick:
						self.tgApi.sendMessage(msg.sender.id, 'You can\'t pick your own picture')
					else:
						# запоминаем Url картинки в которую юзер тыкнул пальцем
						msg.sender.select = self.table[digit - 1]

						if self.allSelected():
							self.broadcastSelects()
							self.broadcastScores()
							self.nextLeader()



