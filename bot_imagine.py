from tgAPI import Telegram
from imagineGame import ImagineGame

tg = Telegram()
games = {}

def onMessage(msg):
	global tg

	if "/game" == msg.text:
		if msg.chatId in games:
			games[msg.chatId].destroy()
		# if not msg.chatId in games:
		games[msg.chatId] = ImagineGame(msg.chatId, tg)
		games[msg.chatId].addPlayer(msg.sender)
			# tools.save_to_json(list(games.keys()), pidorGameChatsFile)


	if msg.stickerId is not None:
		tg.sendMessage(msg.chatId, msg.stickerId)


if __name__ == "__main__":
	print('Starting...')
	tg.addMessageListener(onMessage)
	tg.work()